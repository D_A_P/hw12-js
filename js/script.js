'use strict';

const buttons = document.querySelectorAll('.btn');

document.addEventListener('keydown', function(event) {  
            buttons.forEach(button =>{
            const pressedBtn = event.key.toLowerCase();
            const letterBtn = button.innerText.toLowerCase();    
            
        if( pressedBtn === letterBtn) {
                    button.style.backgroundColor = 'blue';
        } else {
            button.style.backgroundColor = 'black';
        };       

    });
    }
);


